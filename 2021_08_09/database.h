#ifndef DATABASE_H
#define DATABASE_H
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QtSql>
#include <QSqlError>
#include <QObject>
#include <QPixmap>
#include <QQuickView>
#include <QFileDialog>

//class database

class database : public QObject
{
        Q_OBJECT
private:
   QString db_name;

    explicit database( QObject* parent = nullptr );
      virtual ~database();
       void create_table();
       int num_n ;

public:

    QPixmap image;
    Q_INVOKABLE static database * getInstance(QObject*parent =nullptr);
    Q_INVOKABLE QSqlDatabase init_database();
    Q_INVOKABLE void create_database(QString Qstring_db_name);
   Q_INVOKABLE  void delet_data();
    Q_INVOKABLE void insert_database(QString tempp, QString humii, QString rainyy);

   Q_INVOKABLE QPixmap load_img_File();



     //static database* getInstance( QObject* parent = nullptr );


signals:
private slots:
};

#endif // DATABASE_H
