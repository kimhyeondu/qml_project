#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include <qqml.h>
#include "mainview.h"

int main(int argc, char *argv[])
{
  QGuiApplication app (argc,argv);


  QQuickView viewer;

  mainview main_view;



  viewer.engine()->rootContext()->setContextProperty("view",&main_view);
  viewer.setSource(QUrl("qrc:/main.qml"));
    return app.exec();
}
