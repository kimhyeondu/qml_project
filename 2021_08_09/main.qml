import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12// layout을 위해 import


Window {
    id : idWindow
    visible: true
    width: 1000
    height: 1000
    color:"white"
    title: qsTr("database_prog")

Item{
    width:parent.width; height:parent.height
    RowLayout{
        ColumnLayout{
            RowLayout{
                id:row1
                Text {
                    id: temp
                    Layout.fillWidth: true
                    text: qsTr("temp")
                }
                Text{
                    id :humi
                    Layout.fillWidth: true
                    text: qsTr("humi")
                }
                Text{
                    id :rain
                    Layout.fillWidth: true
                    text: qsTr("rain")

                }
            }
            RowLayout{
                id:row2
                TextField{
                    id : temp_edit
                }
                TextField{
                    id :humi_edit
                }
                TextField{
                    id :rain_edit
                }
            }
            RowLayout{
                id:row3
                // anchors.top: row2.bottom


                Button{

                    //  signal insert_button_signal(QString tempp, QString humii, QString rainyy)
                    id: insert_button
                    text: "insert"
                    onClicked: {
                        view.on_insert_button_clicked(temp_edit.text, humi_edit.text, rain_edit.text)
                        //db.getInstance().insert_database(temp_edit.text, humi_edit.text, rain_edit.text)
                        // insert_button_signal(temp_edit.text, humi_edit.text, rain_edit.text)
                    }
                }

                Button{

                    // signal del_buton_siganl()
                    id: del_button
                    //width:Row.width; height:Row.width;
                    text: "delete"
                    onClicked: {
                        view.on_delet_button_clicked()
                      //  db.getInstance().delet_data()
                        // del_buton_siganl()
                    }
                }

                Button{

                    signal show_button_siganl()
                    id:show_button
                    // width:Row.width; height:Row.width;
                    text: "showdata"
//                    onClicked: {
//                        view.on_show_button_clicked(table);
//                    }

                }
            }
            TableView{
                id:table
                objectName: "table"

            }
        }
        ColumnLayout{
            Text{
                id :db_name
                // width: Row.width
                text: qsTr("database_name")
            }
            TextField{
                id : db_edit
                // width:Row.width
            }
            Button{

                //signal bdname_button_siganl()
                id: db_name_button
                text: "create"
                onClicked: {
                    view.on_create_Button_clicked(db_name.text)
                  //  db.getInstance().create_database(db_name.text)
                    //bdname_button_siganl(db_edit.text)
                }
            }
            Button{
                //signal img_button_siganl()
                id: img_load_bueeon

                text: "image"
                onClicked: {
                    view.on_img_button_clicked()
                    //db.getInstance().load_img_File()                            //img_button_siganl()
                }
            }
        }
    }
}
}










