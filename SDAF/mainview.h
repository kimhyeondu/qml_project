#ifndef MAINVIEW_H
#define MAINVIEW_H
#include <QObject>
#include <QQmlProperty>
#include "database.h"
#include"imagesqltablemodel.h"
#include <QTableView>



class mainview :public QObject{
    Q_OBJECT

    public :
    Q_INVOKABLE void on_insert_button_clicked(QString temp,QString humi, QString rain );


    Q_INVOKABLE void on_create_Button_clicked(QString dbname);
    Q_INVOKABLE  void on_delet_button_clicked();
    Q_INVOKABLE ImageSqlTableModel * on_show_button_clicked();
    Q_INVOKABLE  void on_img_button_clicked();
public slots:
    void show_database();
private:
     ImageSqlTableModel  * model;


};




#endif // MAINVIEW_H

