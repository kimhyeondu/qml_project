import QtQuick 2.0

Rectangle {
 id: button
 property alias text : buttonText.text
 Accessible.name: text
 Accessible.role: Accessible.Button
 Accessible.onPressAction: {
     button.clicked()
 }
 signal clicked
 width: buttonText.width + 20
 height: 50
 gradient: Gradient {
     GradientStop { position: 0.0; color: "lightsteelblue" }
     GradientStop { position: 1.0;
         color: button.focus ? "red" : "blue" }
 }
 radius: 5
 antialiasing: true

 MouseArea {
     id: mouseArea
     anchors.fill: parent
     onClicked: parent.clicked()

 }
}
