#ifndef DATABASE_H
#define DATABASE_H
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QtSql>
#include <QSqlError>
#include <QObject>
#include <QPixmap>
#include <QQuickView>
#include <QFileDialog>

//class database

class database : public QObject
{
        Q_OBJECT


private:
   QString db_name;

    explicit database( QObject* parent = nullptr );
      virtual ~database();
       void create_table();
       int num_n ;

public:

    QPixmap image;
    static database * getInstance(QObject*parent =nullptr);
    QSqlDatabase init_database();
    void create_database(QString Qstring_db_name);
     void delet_data();
     void insert_database(QString tempp, QString humii, QString rainyy);

      QPixmap load_img_File();



     //static database* getInstance( QObject* parent = nullptr );


signals:
private slots:
};

#endif // DATABASE_H
