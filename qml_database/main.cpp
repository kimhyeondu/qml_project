#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTableView>
#include "database.h"
#include"imagesqltablemodel.h"
#include <QtSql/QSqlDatabase>
#include<QQuickView>
#include<qqml.h>
#include<mainview.h>
#include <QQuickItem>

class mainview :public QObject{
    Q_OBJECT

    public slots:
     void on_insert_button_clicked(QString temp,QString humi, QString rain );
    void on_pushButton_clicked(QString dbname);
      void on_delet_button_clicked();
     void on_show_button_clicked(QTableView table);
      void on_img_button_clicked();

};

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);


    mainview main_view;
    qmlRegisterType<database>("My_database",1,0,"databasss_class");
    qmlRegisterType<ImageSqlTableModel>("img_tablemodel",1,0,"new_tablemodel");
    QQuickView viewer;


    viewer.setSource(QUrl("qrc:/main.qml"));
    viewer.show();





    QObject *item = viewer.rootObject();

    QObject::connect(item,SIGNAL( insert_button_signal(QString, QString, QString)),
                     &main_view,SLOT(on_insert_button_clicked(QString,QString,Qstring)));

    QObject::connect(item,SIGNAL( del_buton_siganl()),
                     &main_view,SLOT(on_delet_button_clicked()));

    QObject::connect(item,SIGNAL( bdname_button_siganl(QString)),
                     &main_view,SLOT(on_pushButton_clicked(QString)));

    QObject::connect(item,SIGNAL( img_button_siganl()),
                     &main_view,SLOT(on_img_button_clicked()));

    QObject::connect(item,SIGNAL( show_button_siganl(QTableView)),
                     &main_view,SLOT(on_show_button_clicked(QTableView)));
    return app.exec();
}


void mainview::on_insert_button_clicked(QString temp,QString humi, QString rain)
{

database::getInstance()->insert_database(temp, humi, rain);

}


void mainview ::on_pushButton_clicked(QString dbname)
{

database::getInstance()->create_database(dbname);

}

void mainview::on_delet_button_clicked()
{
    database::getInstance()->delet_data();
}

void mainview ::on_show_button_clicked( QTableView table ){

   ImageSqlTableModel  * mode1   =new ImageSqlTableModel(nullptr,database::getInstance()->init_database());
    mode1->setTable("test");
    mode1->setEditStrategy(QSqlTableModel::OnManualSubmit);
    mode1->select();
    table.setModel(mode1);

}

void mainview ::on_img_button_clicked()
{
   database::getInstance()->load_img_File();

}
