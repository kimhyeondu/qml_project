#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include<QQuickView>
#include<QQmlContext>
#include<qqml.h>
#include"database.h"
#include"imagesqltablemodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQuickView viewer;
    database *db;
    viewer.engine()->rootContext()->setContextProperty("database",db);
    viewer.setSource(QUrl("qrc://main.qml"));

    return app.exec();
}
