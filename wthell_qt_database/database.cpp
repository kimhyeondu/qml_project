#include "database.h"
#include <QModelIndex>
#include <QAbstractTableModel>
#include <QStandardItem>
#include<QLoggingCategory>
Q_LOGGING_CATEGORY( db,"test" )

database::database(QObject*parent):QObject(parent)
{
  qDebug() << "hello";
}




database* database::getInstance( QObject* parent )
{
    static database* instance = new database( parent );
    return instance;
}


QSqlDatabase database :: init_database(){

     QSqlDatabase db= QSqlDatabase::database(db_name);
     db.open();
    return db;


}


void database::delet_data(){
    QSqlQuery qy(init_database());
    qy.prepare( "DELETE FROM test "
                "WHERE num = "
                " :num ");
   qy.bindValue(":num",num_n-1);
    if(!qy.exec())
    {

        qDebug()<<qy.lastError();
        qDebug()<<qy.lastQuery();
        qDebug() << "delet error";
    }
   else
    {qDebug () <<"delet!!!";
    num_n -=1;}

}

void database :: create_database(QString Qstring_db_name){
     db_name=Qstring_db_name;
    QSqlDatabase db = QSqlDatabase::addDatabase( "QSQLITE", db_name );
    db.setDatabaseName( db_name +".db" );
     db.open();
     create_table();

}

void database::create_table()
{

    QSqlQuery qy(init_database());
    qy.prepare("CREATE TABLE IF NOT EXISTS test"
                       "("
                       "num      INTEGER  NOT NULL PRIMARY KEY,"
                       "temp     INTEGER NOT NULL,"
                       "humi     INTEGER NOT NULL,"
                       "rain     INTEGER NOT NULL,"
                       "image   BLOB"
                       ");");
    num_n =1;




    if(!qy.exec())
    {
        qDebug()<<qy.lastError();
        qDebug()<<qy.lastQuery();
        qDebug() << "creat error";
    }
    else
        qDebug() << "create!!!!";
////////////////////////////////////////////////////////

}



void database:: insert_database(QString tempp, QString humii, QString rainyy){


        QPixmap pixmap =image;

       // QByteArray image_array;
        //QBuffer image_buffer(&image_array);
     QByteArray image_array;
     QBuffer image_buffer(&image_array);
        image_buffer.open(QIODevice::WriteOnly);
        pixmap.save(&image_buffer,"PNG");

    QSqlQuery qy(init_database());
      qy.prepare("INSERT INTO test"
                 "('num' ,'temp','humi','rain','image')"
    //               "('num' ,'temp','humi','rain')"
                  "VALUES"
   //               "( ?, ?,?,?)");
                "(?, ?, ?,?,?)");
      qy.bindValue(0,num_n);
      qy.bindValue(1,tempp);
      qy.bindValue(2,humii);
      qy.bindValue(3,rainyy);
      qy.bindValue(4,image_array);

         num_n+=1;
      if(!qy.exec())
      {
          qDebug()<<qy.lastError();
          qDebug()<<qy.lastQuery();
          qDebug() << "insert error";
      }
      else

               qDebug () <<"inset!!!";

   }




QPixmap database :: load_img_File(){

   // QString img_path = QFileDialog::getExistingDirectory(nullptr,"search Folder",QDir::homePath(),QFileDialog::ShowDirsOnly);
    //QString img_path = QFileDialog::getOpenFileName(nullptr, "Open File",
      //                                              "/home",
        //                                            "Images (*.png *.xpm *.jpg)");
    QPixmap pixmap_image;
    pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                   "/home",
                                                  "Images (*.png *.xpm *.jpg)"));

    image=pixmap_image;
  /*  QStringList file_name = QFileDialog::getOpenFileNames(
                            nullptr,
                            "Select one or more files to open",
                            "/home",
                            "Images (*.png *.xpm *.jpg)");
*/

    return pixmap_image;
}

database :: ~database(){


}





