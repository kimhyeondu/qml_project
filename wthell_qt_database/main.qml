import QtQuick 2.12
import QtQuick.Window 2.12
import My_database 1.0
import img_tablemodel 1.0
import QtQuick.Controls 1.4

Window {
    visible: true
    width: 445
    height: 445
    color:"white"
    title: qsTr("database_prog")
//    Loader{
//        id:mainpage
//        anchors.top: data_show.bottom
//    }
    Item{
        Column{
            Row{
                Row{
                    Column{
                        Text {
                            id: temp
                            width: Row.width
                            text: qsTr("temp")
                        }
                        Text{
                            id :humi
                            width: Row.width
                            text: qsTr("humi")
                        }
                        Text{
                            id :rain
                            width: Row.width
                            text: qsTr("rain")
                        }
                    }
                    Column{
                        TextEdit{
                            id : temp_edit
                            width:Row.width
                        }
                        TextEdit{
                            id :humi_edit
                            width:Row.width
                        }
                        TextEdit{
                            id :rain_edit
                            width:Row.width
                        }

                    }
                }
                Column{
                    Button{

                        signal insert_button_signal(QString tempp, QString humii, QString rainyy)
                        id: insert_button
                        width:Column.width; height:Column.width; text: "insert"
                        onClicked: {

                            insert_button_signal(temp_edit.text, humi_edit.text, rain_edit.text)
                        }
                    }


                    Button{
                        signal del_buton_siganl()
                        id: del_button
                        width:Column.width; height:Column.width; text: "delete"
                        onClicked: {
                            del_buton_siganl()
                        }
                    }

                    Button{
                        signal show_button_siganl()
                        id:show_button
                        width:Column.width; height:Column.width; text: "showdata"
                        onClicked:{
                            show_button_siganl(table)

                        }
                    }
                    TableView{
                        id : table

                        anchors.fill:parent
                    }
                }
                Row{
                    Text{
                        id :db_name
                        width: Row.width
                        text: qsTr("database_name")
                    }
                    TextEdit{
                        id : db_edit
                        width:Row.width
                    }

                    Button{
                        signal bdname_button_siganl()
                        id: db_name_button
                        width:Column.width; height:Column.width; text: "delete"
                        onClicked: {
                           bdname_button_siganl(db_edit.text)
                        }
                    }
                    Button{
                        signal img_button_siganl()
                        id: img_load_bueeon
                        width:Column.width; height:Column.width; text: "delete"
                        onClicked: {
                            img_button_siganl()
                        }
                    }
                }
            }
        }
    }
}













